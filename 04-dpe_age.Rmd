# `r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["titre"]]` {#dpe}

## `r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["intertitre"]]` {.unnumbered}

`r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["commentaires"]]`



```{r, warning=FALSE, message=FALSE}

tableau_4_1 <- creer_tableau_4_1(data = indicateurs_rpls_ref, annee = annee, epci = FALSE)
exports_xls <- incrementer_export_xls(tableau_4_1, list_xls = exports_xls)
afficher_visuel(tableau_4_1)

```

```{r DPE diag barres}

graphe_4_1 <- creer_graphe_4_1(data = indicateurs_rpls_ref, annee = annee)
exports_xls <- incrementer_export_xls(graphe_4_1, list_xls = exports_xls)
afficher_visuel(graphe_4_1)

```

